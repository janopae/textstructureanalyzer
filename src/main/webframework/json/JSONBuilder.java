package webframework.json;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class JSONBuilder {

	public static String buildJSON(double rate, String filename) {
		String result = "";
		
		String report_status = "";
		
		if (rate <= 30) {
			report_status = "red";
		} else if (rate <= 80) {
			report_status = "yellow";
		} else if (rate <= 100) {
			report_status = "green";
		}
		
		result = String.format("{\r\n" + "  \"analyser_name\": \"textstructureanalyzer\",\r\n"
				+ "  \"analyser_description\": \"Analyzes any text on its readability. (Just html implemented by now)!\",\r\n"
				+ "  \"report_title\": \"HMTL readability of %s\",\r\n" + "  \"report_analysedObject\": \"%s\",\r\n"
				+ "  \"report_timestamp\": \"%s\",\r\n" + "  \"report_status\": \"%s\",\r\n"
				+ "  \"report_items\": [%s]\r\n" + "}", filename, filename,
				LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME), rate,report_status);
		return result;
	}
}
