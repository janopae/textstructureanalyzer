package webframework;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Sender {
	private URL url;

	public Sender(String url) {
		try {
			this.url = new URL(url);
		} catch (MalformedURLException e) {

			e.printStackTrace();
		}
	}

	public void send(String json) {
		HttpURLConnection connection;
		try {
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json; utf-8");
			connection.setDoOutput(true);

			try (OutputStream os = connection.getOutputStream()) {
				byte[] input = json.getBytes("utf-8");
				os.write(input, 0, input.length);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
