package Main;

import Gui.Controller;
import Gui.JFrameView;
import Parser.HtmlSectionParser.HTMLSectionParser;
import Parser.SectionParserInterface;
import Rater.DefaultRater;
import Rater.RaterInterface;

public class Program {
    public static void main(String[] args) {
        JFrameView view = new JFrameView();
        SectionParserInterface sectionParser = new HTMLSectionParser();
        RaterInterface rater = new DefaultRater();

        new Controller(view, sectionParser, rater);
    }
}