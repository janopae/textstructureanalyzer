package Parser;

import Parser.Exception.ParseException;
import Parser.Exception.SectionTreeBuildingException;

public interface SectionParserInterface {
    Section parse(String raw) throws ParseException, SectionTreeBuildingException;
}