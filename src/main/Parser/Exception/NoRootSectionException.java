package Parser.Exception;

public class NoRootSectionException extends SectionTreeBuildingException {
    public NoRootSectionException(String message) {
        super(message);
    }
}
