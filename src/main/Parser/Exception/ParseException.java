package Parser.Exception;

/**
 * Exception thrown by a SectionParser implementation if there's an error regarding the markup language the
 * implementation parses. E. g. if the markup requires each tag to be closed, this exception can be thrown if there
 * is an unclosed tag. Don't forget to provide an expressive message, as the exception type itself does not tell that
 * much about the actual issue.
 */
public class ParseException extends Exception {
	private final int position;

    public ParseException(int position, String message) {
        super(message);
        this.position = position;
    }

    public int getPosition() {
        return this.position;
    }

}
