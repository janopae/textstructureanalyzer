package Parser.Exception;

/**
 * Exception thrown by a SectionParser implementation, if there is no unique level 1 heading for the whole document.
 */
public class MultipleRootSectionsException extends SectionTreeBuildingException {
    public MultipleRootSectionsException(String message) {
        super(message);
    }
}
