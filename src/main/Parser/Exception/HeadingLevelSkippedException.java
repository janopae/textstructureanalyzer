package Parser.Exception;

/**
 * Exception thrown by a SectionParser implementation, if a hading level has been skipped, e. g. if a level 4 heading
 * follows immediately to a level 2 heading.
 */
public class HeadingLevelSkippedException extends SectionTreeBuildingException {
    public HeadingLevelSkippedException(String message) {
        super(message);
    }
}
