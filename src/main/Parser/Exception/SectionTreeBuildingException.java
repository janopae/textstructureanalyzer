package Parser.Exception;

public abstract class SectionTreeBuildingException extends Exception {
    public SectionTreeBuildingException(String message) {
        super(message);
    }
}
