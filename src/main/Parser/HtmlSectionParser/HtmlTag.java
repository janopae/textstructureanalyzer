package Parser.HtmlSectionParser;

public enum HtmlTag {
    P,
    H1,
    H2,
    H3,
    H4,
    H5,
    H6,
    H7,
    H8,
    H9;

    static HtmlTag fromString(String string) {
        for (HtmlTag tag : HtmlTag.values()) {
            if (string.equalsIgnoreCase(tag.toString())) {
                return tag;
            }
        }

        return null;
    }
}
