package Parser.HtmlSectionParser;

import Parser.Exception.*;
import Parser.HtmlSectionParser.Tokeniser.Token;
import Parser.HtmlSectionParser.Tokeniser.TokenType;
import Parser.HtmlSectionParser.Tokeniser.Tokeniser;
import Parser.Section;
import Parser.SectionTreeBuilder;
import Parser.SectionParserInterface;
import java.util.List;
import java.util.Stack;

public class HTMLSectionParser implements SectionParserInterface {

    private final Tokeniser tokeniser;

    public HTMLSectionParser(Tokeniser tokeniser) {
        this.tokeniser = tokeniser;
    }

    public HTMLSectionParser() {
        this.tokeniser = new Tokeniser();
    }

    public Section parse(String raw) throws ParseException, SectionTreeBuildingException {
        char[] rawChars = raw.toCharArray();
        List<Token> tokenStream = tokeniser.getTokenStream(rawChars);
        this.removeUnsupportedTags(tokenStream);

        Stack<HtmlTag> openedTags = new Stack<>();
        SectionTreeBuilder sectionBuilder = new SectionTreeBuilder();

        for (Token currentToken : tokenStream) {
            switch (currentToken.type) {
                case OPENING_TAG -> {
                    HtmlTag tag = HtmlTag.fromString(currentToken.content);

                    openedTags.push(tag);
                }
                case CLOSING_TAG -> {
                    HtmlTag tag = HtmlTag.fromString(currentToken.content);

                    if (openedTags.empty() || openedTags.peek() != tag) {
                        throw new ParseException(currentToken.originalTextPosition, "Tag " + tag.toString() + " closed, but hasn't been opened");
                    }

                    openedTags.pop();
                }
                case TEXT -> {
                    if (openedTags.empty()) {
                        continue; // ignore Text without tags - this includes whitespace as well as text between unsupported tags (like <head><title>) that have been sorted out
                    }
                    switch (openedTags.peek()) {
                        case P -> sectionBuilder.addParagraphToCurrentSection(currentToken.content);
                        case H1, H2, H3, H4, H5, H6, H7, H8, H9 -> {
                            int headingLevel =  Integer.parseInt(openedTags.peek().toString().substring(1));
                            Section newSection = new Section(currentToken.content);

                            sectionBuilder.pushSection(headingLevel, newSection);
                        }
                    }
                }
            }
        }

        if (!openedTags.empty()) {
            throw new ParseException(raw.length() - 1, "Tag " + openedTags.peek().toString() + " hasn't been closed");
        }

        return sectionBuilder.getRootSection();
    }

    private void removeUnsupportedTags(List<Token> tokenStream) {
        for (Token token: tokenStream.toArray(new Token[0])) {
            if (token.type != TokenType.OPENING_TAG && token.type != TokenType.CLOSING_TAG) {
                continue;
            }

            HtmlTag tag = HtmlTag.fromString(token.content);
            if (tag != null) {
                continue; // tag is supported - we can continue
            }

            int index = tokenStream.indexOf(token);
            tokenStream.remove(token);

            // If the unsupported tag separates two TEXT tokens from one another (like "<p>text<unsupported>text</p>"), the TEXT tokens need to be merged
            int precedingIndex = index - 1;
            int nextIndex = index;
            if (precedingIndex < 0 || nextIndex >= tokenStream.size()) {
                continue;
            }

            Token token1 = tokenStream.get(precedingIndex);
            Token token2 = tokenStream.get(nextIndex);

            if (token1.type == TokenType.TEXT && token2.type == TokenType.TEXT) {
                String content = token1.content + token2.content;
                Token mergedToken = new Token(token1.type, token1.originalTextPosition, content);
                tokenStream.add(precedingIndex, mergedToken);
                tokenStream.remove(token1);
                tokenStream.remove(token2);
            }

        }
    }
}