package Parser.HtmlSectionParser.Tokeniser;

import Parser.Exception.ParseException;

import java.util.List;

public class Tokeniser {
    private enum ParsingState {
        INIT,
        TEXT,
        TAG,
        OPENING_TAG,
        CLOSING_TAG,
        TAG_PARAMETERS
    }

    private ParsingState state;

    public List<Token> getTokenStream(char[] rawChars) throws ParseException {
        TokenStreamBuilder tokenStreamBuilder = new TokenStreamBuilder();

        this.state = ParsingState.INIT;
        for (int i = 0; i < rawChars.length; i++) {
            char currentChar = rawChars[i];

            switch (this.state) {
                case INIT -> {
                    switch (currentChar) {
                        case '<' -> this.state = ParsingState.TAG;
                        default -> {
                            this.state = ParsingState.TEXT;
                            tokenStreamBuilder.appendNewToken(TokenType.TEXT, i);
                            tokenStreamBuilder.appendChar(currentChar);
                        }
                    }
                }
                case TEXT -> {
                    switch (currentChar) {
                        case '<' -> this.state = ParsingState.TAG;
                        default -> tokenStreamBuilder.appendChar(currentChar);
                    }
                }
                case TAG -> {
                    switch (currentChar) {
                        case '/' -> {
                            this.state = ParsingState.CLOSING_TAG;
                            tokenStreamBuilder.appendNewToken(TokenType.CLOSING_TAG, i);
                        }
                        default -> {
                            this.state = ParsingState.OPENING_TAG;
                            tokenStreamBuilder.appendNewToken(TokenType.OPENING_TAG, i);
                            tokenStreamBuilder.appendChar(currentChar);
                        }
                    }
                }
                case OPENING_TAG -> {
                    switch (currentChar) {
                        case '<' -> throw new ParseException(i, "Tag incomplete");
                        case ' ' -> this.state = ParsingState.TAG_PARAMETERS;
                        case '>' -> this.state = ParsingState.INIT;
                        default ->  tokenStreamBuilder.appendChar(currentChar);
                    }
                }
                case CLOSING_TAG -> {
                    switch (currentChar) {
                        case '<' -> throw new ParseException(i, "Tag incomplete");
                        case ' ' -> throw new ParseException(i, "Spaces are not allowed in closing tags");
                        case '>' -> this.state = ParsingState.INIT;
                        default ->  tokenStreamBuilder.appendChar(currentChar);
                    }
                }
                case TAG_PARAMETERS -> {
                    switch (currentChar) {
                        case '>' -> this.state = ParsingState.INIT;
                    }
                }
            }
        }

        if (this.state != ParsingState.INIT) {
            // TODO: throw error
        }

        return tokenStreamBuilder.getTokenStream();
    }
}
