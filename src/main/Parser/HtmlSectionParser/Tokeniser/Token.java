package Parser.HtmlSectionParser.Tokeniser;

public class Token {
    public final TokenType type;
    public final int originalTextPosition;
    public final String content;

    public Token(TokenType type, int originalTextPosition, String content) {
        this.type = type;
        this.content = content;
        this.originalTextPosition = originalTextPosition;
    }
}
