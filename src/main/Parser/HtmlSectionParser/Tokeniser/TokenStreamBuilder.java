package Parser.HtmlSectionParser.Tokeniser;

import java.util.List;
import java.util.Stack;

public class TokenStreamBuilder {
    private final Stack<Token> tokenStack;

    private TokenType currentTokenType;
    private StringBuilder currentTokenContent;
    private int currentTokenOriginalTextPosition;

    public TokenStreamBuilder() {
        tokenStack = new Stack<>();
        currentTokenContent = new StringBuilder();
    }

    /**
     * Appends a character to the last token in the TokenStream.
     */
    public void appendChar(char c) {
        currentTokenContent.append(c);
    }

    /**
     * Appends a new Token of the given type to the TokenStream.
     */
    public void appendNewToken(TokenType type, int originalTextPosition) {
        if (currentTokenType != null) {
            tokenStack.add(new Token(currentTokenType, currentTokenOriginalTextPosition, currentTokenContent.toString()));
        }

        currentTokenType = type;
        currentTokenOriginalTextPosition = originalTextPosition;
        currentTokenContent = new StringBuilder();
    }

    public List<Token> getTokenStream() {
        if (currentTokenType != null) {
            tokenStack.add(new Token(currentTokenType, currentTokenOriginalTextPosition, currentTokenContent.toString()));
        }
        return tokenStack;
    }
}
