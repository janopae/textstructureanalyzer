package Parser.HtmlSectionParser.Tokeniser;

public enum TokenType {
    OPENING_TAG,
    CLOSING_TAG,
    TEXT
}
