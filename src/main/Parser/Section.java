package Parser;

import java.util.ArrayList;
import java.util.List;

public class Section {
    // Attribute
    private String title;
    private List<String> paragraphs;
    private List<Section> subsections;

    public Section(String title) {
        this.title = title;
        paragraphs = new ArrayList<String>();
        subsections = new ArrayList<Section>();
    }

    public Section() {
        paragraphs = new ArrayList<String>();
        subsections = new ArrayList<Section>();
    }

    // Methoden
    public void addSubsection(Section subSection) {
        subsections.add(subSection);
    }

    public void addParagraph(String paragraph) {
        paragraphs.add(paragraph);
    }

    // Getter und Setter
    public String getTitle() {
        return title;
    }

    public Section[] getSubsections() {
        return subsections.toArray(new Section[subsections.size()]);
    }

    public String[] getParagraphs() {
		return paragraphs.toArray(new String[paragraphs.size()]);
    }

    @Override
    public String toString() {
        String result = title + "\n";
        for (int i = 0; i < paragraphs.size(); i++) {
            result += paragraphs.get(i) + "\n";
        }
        if (subsections.size() > 0) {
            result += "----Subsections von " + title + "----\n";
        }
        for (int i = 0; i < subsections.size(); i++) {
            result += subsections.get(i).toString();
        }
        return result;
    }
}
