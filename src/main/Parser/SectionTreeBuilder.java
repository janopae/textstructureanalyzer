package Parser;

import Parser.Exception.HeadingLevelSkippedException;
import Parser.Exception.MultipleRootSectionsException;
import Parser.Exception.NoRootSectionException;

import java.util.Stack;

public class SectionTreeBuilder {
    /**
     * Actually one Section above the root section. Maybe this can be fixed one day.
     */
    private final Section rootSection;
    /**
     * Stack that contains the Section that is currently built at its top, and the parent sections below. Allows
     * modifying the current Section and returning to its parents by popping the stack.
     */
    private final Stack<Section> currentSectionStack;
    /**
     * Keeps track of the current heading level. Maybe can be replaced with currentSectionStack.size()?
     */
    private int currentHeadingLevel;

    public SectionTreeBuilder() {
        currentSectionStack = new Stack<>();
        rootSection = new Section();
        currentSectionStack.push(rootSection);
        currentHeadingLevel = 0;
    }

    public Section getRootSection() throws MultipleRootSectionsException, NoRootSectionException {
        if (rootSection.getSubsections().length == 0) {
            throw new NoRootSectionException("You need to push at least one Section to the tree in order to retrieve one.");
        }

        if (rootSection.getSubsections().length != 1) {
            throw new MultipleRootSectionsException("There must be exactly one top-level heading");
        }

        return rootSection.getSubsections()[0];
    }

    public void addParagraphToCurrentSection(String paragraph) {
        currentSectionStack.peek().addParagraph(paragraph);
    }

    public void pushSection(int headingLevel, Section section) throws HeadingLevelSkippedException {
        if (headingLevel < 1) {
            // explode!!
        }

        if (headingLevel > currentHeadingLevel + 1) {
            String message = getErrorMessageForHeadingLevelSkippedException(headingLevel, section.getTitle());
            throw new HeadingLevelSkippedException(message);
        }

        if (headingLevel < currentHeadingLevel + 1) {
            rewindCurrentSectionToHeadingLevel(headingLevel - 1);
        }

        appendSectionToCurrentSection(section);
    }

    private void rewindCurrentSectionToHeadingLevel(int headingLevel) {
        while (currentHeadingLevel > headingLevel)  {
            currentSectionStack.pop();
            currentHeadingLevel--;
        }
    }

    private void appendSectionToCurrentSection(Section section) {
        currentSectionStack.peek().addSubsection(section);
        currentSectionStack.push(section);
        currentHeadingLevel++;
    }

    /**
     * @param headingLevel heading level of the Section that skips heading levels
     * @param title title of the Section that skips heading levels
     */
    private String getErrorMessageForHeadingLevelSkippedException(int headingLevel, String title) {
        int headingLevelsSkipped = headingLevel - (currentHeadingLevel + 1);
        String currentHeadingTitle = currentSectionStack.peek().getTitle();

        return String.format(
                "Skipped %o %s: Level %o heading \"%s\" follows on level %o heading \"%s\".",
                headingLevelsSkipped,
                headingLevelsSkipped > 1 ? "heading levels" : "heading level",
                headingLevel,
                title,
                currentHeadingLevel,
                currentHeadingTitle
        );
    }
}
