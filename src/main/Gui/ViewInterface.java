package Gui;

public interface ViewInterface {
    /**
     * Sets a handler to handle functionality that is invoked by interaction with the view at runtime,
     * e. g. by button clicks.
     */
    void setActionHandler(ActionHandler actionHandler);

    /**
     * Makes the view appear or disappear. Make sure to set an actionHandler before setting visible to true, otherwise
     * interaction with the view might lead to errors.
     */
    void setVisible(boolean visible);

    /**
     * Informs the user about a rating result.
     */
    void displayRatingResult(double ratingResult);

    /**
     * Indicates a syntax error in the input text.
     */
    void indicateSyntaxError(SyntaxError exception);

    /**
     * Displays an error message to the user.
     */
    void alertError(Exception exception);
}
