package Gui;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;
import java.awt.*;

public class JFrameView extends JFrame implements ViewInterface {
    private ActionHandler actionHandler;

    private JTextPane mainTextInput;
    private JTextArea syntaxErrorArea;
    private JButton submitInputForRatingButton;

    public JFrameView() {
        super("Scientific text structure analyzing tool");
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.buildWindowContent();
        this.indicateSyntaxError(null);
    }

    /**
     * ------------------------------------------------------
     * |                        | syntaxErrorAreaDescription|
     * |                        |---------------------------|
     * |                        | syntaxErrorArea           |
     * |                        |                           |
     * |                        |                           |
     * |     mainTextInput      |                           |
     * |                        |                           |
     * |                        |                           |
     * |                        |----------------------------
     * |                        |submitInputForRatingButton |
     * ------------------------------------------------------
     *
     */
    private void buildWindowContent() {
        this.setSize(400, 430);
        this.setLayout(null);

        JTextPane mainTextInput = new JTextPane();
        mainTextInput.setBounds(0,0,200,400);
        this.add(mainTextInput);
        this.mainTextInput = mainTextInput;
        mainTextInput.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent documentEvent) {
                actionHandler.validateInputAction(mainTextInput.getText());
            }

            @Override
            public void removeUpdate(DocumentEvent documentEvent) {
                actionHandler.validateInputAction(mainTextInput.getText());
            }

            @Override
            public void changedUpdate(DocumentEvent documentEvent) {
                actionHandler.validateInputAction(mainTextInput.getText());
            }
        });

        JLabel syntaxErrorAreaDescription = new JLabel("Syntax errors:");
        syntaxErrorAreaDescription.setBounds(200, 0, 200, 20);
        this.add(syntaxErrorAreaDescription);

        JTextArea syntaxErrorArea = new JTextArea();
        syntaxErrorArea.setBounds(200, 20, 200, 360);
        syntaxErrorArea.setEditable(false);
        syntaxErrorArea.setCursor(null);
        syntaxErrorArea.setOpaque(false);
        syntaxErrorArea.setFocusable(false);
        syntaxErrorArea.setLineWrap(true);
        this.add(syntaxErrorArea);
        this.syntaxErrorArea = syntaxErrorArea;

        JButton submitInputForRatingButton = new JButton("Rate");
        submitInputForRatingButton.setBounds(200, 380, 200, 20);
        this.add(submitInputForRatingButton);
        submitInputForRatingButton.addActionListener(actionEvent -> {
            if (this.actionHandler == null) {
                throw new RuntimeException("View not initialized correctly. You should set the ActionHandler before setting the view visible. Check your code.");
            }
            this.actionHandler.submitInputForRatingAction(mainTextInput.getText());
        });
        this.submitInputForRatingButton = submitInputForRatingButton;
    }

    @Override
    public void setActionHandler(ActionHandler actionHandler) {
        this.actionHandler = actionHandler;
    }

    @Override
    public void indicateSyntaxError(SyntaxError error) {
        if (error == null) {
            submitInputForRatingButton.setEnabled(true);
            syntaxErrorArea.setText("✓ No syntax errors.");
            syntaxErrorArea.setForeground(Color.GREEN);
            return;
        }
        submitInputForRatingButton.setEnabled(false);
        int position = error.position;
        String message = error.message;

        syntaxErrorArea.setText(position + ": " + message);
        syntaxErrorArea.setForeground(Color.RED);

        // highlight erroneous character in input pane
        Highlighter highlighter = this.mainTextInput.getHighlighter();
        highlighter.removeAllHighlights();
        if (position == 0) {
            return; // we cannot highlight stuff at position 0
        }
        try {
            highlighter.addHighlight(position-1, position, new DefaultHighlighter.DefaultHighlightPainter(Color.RED));
        } catch (BadLocationException e) {
            e.printStackTrace(); // this Exception does not need any proper handling. It just means there's nothing to highlight, why should I care?
        }
    }

    @Override
    public void alertError(Exception exception) {
        JDialog errorDialog = new JDialog();
        errorDialog.setTitle("Error");
        errorDialog.add(new JLabel(exception.getMessage()));
        errorDialog.setModal(true);
        errorDialog.setSize(200, 80);
        errorDialog.setVisible(true);
    }

    @Override
    public void displayRatingResult(double ratingResult) {
        JDialog resultDialog = new JDialog();
        resultDialog.setTitle("Parsing result");
        resultDialog.add(new JLabel("Your text's score: " + ratingResult));
        resultDialog.setModal(true);
        resultDialog.setSize(200, 80);
        resultDialog.setVisible(true);
    }
}
