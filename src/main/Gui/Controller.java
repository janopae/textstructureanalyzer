package Gui;


import Parser.Exception.ParseException;
import Parser.Exception.SectionTreeBuildingException;
import Parser.Section;
import Parser.SectionParserInterface;
import Rater.RaterInterface;

public class Controller implements ActionHandler {
    private SectionParserInterface sectionParser;
    private RaterInterface rater;

    private ViewInterface mainView;

    public Controller(ViewInterface mainView, SectionParserInterface sectionParser, RaterInterface rater) {
        this.mainView = mainView;
        this.mainView.setActionHandler(this);
        this.mainView.setVisible(true);

        this.sectionParser = sectionParser;
        this.rater = rater;
    }

    @Override
    public void submitInputForRatingAction(String input) {
        Section document;
        try {
            document = sectionParser.parse(input);
        } catch (Exception exception) {
            mainView.alertError(exception);
            return;
        }
        double ratingResult = rater.rate(document);
        // TODO: Send stuff to JSON endpoint
        mainView.displayRatingResult(ratingResult);
    }

    @Override
    public void validateInputAction(String input) {
        try {
            sectionParser.parse(input);
        } catch (ParseException exception) {
            SyntaxError error = new SyntaxError(exception.getPosition(), exception.getMessage());
            mainView.indicateSyntaxError(error);
            return;
        } catch (SectionTreeBuildingException exception) {
            SyntaxError error = new SyntaxError(0, exception.getMessage());
            mainView.indicateSyntaxError(error);
            return;
        } catch (RuntimeException exception) {
            mainView.alertError(exception);
            return;
        }
        mainView.indicateSyntaxError(null);
    }
}
