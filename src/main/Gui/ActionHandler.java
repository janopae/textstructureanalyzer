package Gui;

public interface ActionHandler {
    void submitInputForRatingAction(String input);
    void validateInputAction(String input);
}
