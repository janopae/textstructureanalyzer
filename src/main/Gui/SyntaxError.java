package Gui;

public class SyntaxError {
    public final int position;
    public final String message;

    public SyntaxError(int position, String message) {
        this.position = position;
        this.message = message;
    }
}
