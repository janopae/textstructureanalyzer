package Rater;

import Parser.Section;

public interface RaterInterface {
    double rate(Section documentToRate);
}
