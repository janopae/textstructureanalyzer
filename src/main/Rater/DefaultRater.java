package Rater;

import Parser.Section;

public class DefaultRater implements RaterInterface{
	private int maxParagraphLength;

	private int paragraphCount;
	private double score;

	public DefaultRater(int maxParagraphLength){
		this.maxParagraphLength = maxParagraphLength;
		paragraphCount = 0;
		score = 0;
	}

	public DefaultRater(){
		maxParagraphLength = 500;
		paragraphCount = 0;
		score = 0;
	}

    public double rate(Section documentToRate){
		for (String paragraph : documentToRate.getParagraphs()){
			paragraphCount++;
			if (paragraph.length() > maxParagraphLength){
				score += maxParagraphLength / paragraph.length();
			} else {
				score += 100;
			}
		}
		for (Section section : documentToRate.getSubsections()){
			rate(section);
		}
		return score/paragraphCount;
	}
}
