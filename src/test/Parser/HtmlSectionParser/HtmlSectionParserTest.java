package Parser.HtmlSectionParser;

import Parser.Exception.ParseException;
import Parser.Exception.SectionTreeBuildingException;
import Parser.Section;
import org.junit.Before;
import org.junit.Test;
import static junit.framework.TestCase.assertEquals;

/**
 * The original test string has been this:
 * String html = "<h1>Überschrift 1</h1><p>Absatz.</p><p>Absatz2.</p><h2>Storys fürs Leben</h2><h3>Erstes Kapitel</h3><p>Es war einmal ein hässlicher Blaubarsch.</p><h3>Zweites Kapitel</h3><p>Der war so hässlich, dass alle Leute gestorben sind.</p><p>Ende.</p><h2>Danksagung</h2><p>Danke an Patrick für diese unfassbare Weisheit.</p>";
 *
 * It's too complicated to write readable unit tests, but I like it, so I keep it in this comment.
 */
public class HtmlSectionParserTest {
    private HTMLSectionParser parser;

    @Before
    public void setUp() {
        this.parser = new HTMLSectionParser();
    }

    @Test
    public void parsesInputWithOneHeadingAndOneParagraph() throws ParseException, SectionTreeBuildingException {
        String html = "<h1>First Heading</h1><p>First paragraph</p>";
        Section expectedResult = new Section("First Heading");
        expectedResult.addParagraph("First paragraph");

        Section result = parser.parse(html);

        assertEquals(expectedResult.toString(), result.toString());
    }

    @Test
    public void parsesInputWithMultipleHeadingLevels() throws ParseException, SectionTreeBuildingException {
        String html = "<h1>First Heading</h1><h2>First Subheading</h2><h3>First Subsubheading</h3><p>First paragraph</p><h2>Second Subheading</h2><p>Second paragraph</p>";

        Section expectedResult = new Section("First Heading");

        Section firstSubsection = new Section("First Subheading");
        expectedResult.addSubsection(firstSubsection);

        Section firstSubsubsection = new Section("First Subsubheading");
        firstSubsubsection.addParagraph("First paragraph");
        firstSubsection.addSubsection(firstSubsubsection);

        Section secondSubsection = new Section("Second Subheading");
        secondSubsection.addParagraph("Second paragraph");
        expectedResult.addSubsection(secondSubsection);

        Section result = parser.parse(html);

        assertEquals(expectedResult.toString(), result.toString());
    }

    @Test
    public void parsesInputWithTagsItDoesntKnow() throws ParseException, SectionTreeBuildingException {
        String html = "<h1>First Heading</h1><p><b>First</b> paragraph</p>";
        Section expectedResult = new Section("First Heading");
        expectedResult.addParagraph("First paragraph");

        Section result = parser.parse(html);

        assertEquals(expectedResult.toString(), result.toString());
    }

    @Test(expected = ParseException.class)
    public void throwsParseExceptionIfTagHasNotBeenClosed() throws ParseException, SectionTreeBuildingException {
        String html = "<h1>Test";

        parser.parse(html);
    }

    @Test()
    public void throwsNoExceptionIfTextIstSurroundedByUnknownTags() throws ParseException, SectionTreeBuildingException {
        String html = "<head><title>hello</title></head><h1>Test</h1>";

        parser.parse(html);
    }

    @Test(expected = SectionTreeBuildingException.class)
    public void throwsExceptionIfThereIsNoH1() throws ParseException, SectionTreeBuildingException {
        String html = "<h2>Second Heading</h2><p>First paragraph</p>";

        parser.parse(html);
    }

    @Test(expected = SectionTreeBuildingException.class)
    public void throwsExceptionIfHeadingLevelIsSkipped() throws ParseException, SectionTreeBuildingException {
        String html = "<h1>First heading</h1><h3>Third heading/h3>";

        parser.parse(html);
    }

    @Test(expected = ParseException.class)
    public void throwsParseExceptionIfTagIsIncomplete() throws ParseException, SectionTreeBuildingException {
        String html = "<h1>Test</h1><h2</h2>";

        parser.parse(html);
    }

    @Test(expected = ParseException.class)
    public void throwsParseExceptionIfClosingTagContainsSpaces() throws ParseException, SectionTreeBuildingException {
        String html = "<h1>Test</h1 lol>";

        parser.parse(html);
    }
}