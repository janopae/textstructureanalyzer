# TextSctructureAnalyzer

A document analyzer for rating the readability of a structured document.

## Compiling

Clone the repository and use gradle to compile the build task:
```
cd cloned-repository-directory
./gradlew jar
```

You can now execute the .jar file, which for some reason is in the build/lib
directory.

## Usage

The main window lets you type in a text formatted with `<p>`  and `<h>` tags. A
panel on the right will inform you about syntax errors while typing.

Our current parsing implementation will throw unexpected errors when you
start a tag without ending it. If you don't want to click away a bunch
of error messages, type the ending bracket first or copy and paste
your HTML from a text editor.

After typing in your text, you can click on the "Rate" button to receive
your text structure readability rating score. This only works if the
syntax is correct.

## Supported input formats

Currently, the TextStructureAnalyzer only supports HTML formatted text input with \<p\> and \<h\> tags.
